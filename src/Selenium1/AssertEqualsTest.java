package Selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AssertEqualsTest {

	static WebDriver driver;



	@BeforeClass

	public void beforeClass() {

		// To launch a Chrome Browser

		System.setProperty("webdriver.chrome.driver", "/Users/bjahagirdar/Downloads/chromedriver");

		driver = new ChromeDriver();

	}



	@Test

	public void test1() {

		// To launch a Chrome Browser

		driver.navigate().to("https://www.spicejet.com/");

		WebElement element = driver.findElement(By.id("divpaxinfo"));

		String actualText = element.getText();

		String expectedResult = "1 Adult";

		System.out.println(driver.findElement(By.id("divpaxinfo")).getText());

		Assert.assertEquals(actualText, expectedResult);



		System.out.println("Text After Assertions :)");



	}



	@Test

	public void test2() {

		driver.navigate().to("https://www.spicejet.com/");

		String actualText = "1 Adult";

		String expectedResult = "1 Adult";

		Assert.assertEquals(actualText, expectedResult);



		System.out.println("Text After Assertions :)");

	}

}
