package Selenium1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirstClass {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver_win32\\chromedriver.exe"); // Change
																												// the
																												// path
																												// here
		// accordingly

		WebDriver driver = new ChromeDriver();
		driver.get("https://www.google.com/");
		driver.manage().window().maximize();
		String appTitle = driver.getTitle();
		String expTitle = "Google";
		if (appTitle.equals(expTitle)) {
			System.out.println("Verification Successful");
		} else {
			System.out.println("Verification Failed");
		}
		driver.close();
		System.exit(0);
	}
}
