package Selenium1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.IRetryAnalyzer;
import org.testng.annotations.ITestAnnotation;
import org.testng.annotations.Test;

public class FailureRetryListener implements IAnnotationTransformer {

	// Overriding the transform method to set the RetryAnalyzer
	public void transform(ITestAnnotation testAnnotation, Class testClass, Constructor testConstructor,
	Method testMethod) {
	//IRetryAnalyzer retry = testAnnotation.getRetryAnalyzerClass();
	//if (retry == null)
	testAnnotation.setRetryAnalyzer(RetryAnalyser.class);
	}
	}