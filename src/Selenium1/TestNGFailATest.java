package Selenium1;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestNGFailATest {

@Test
public void testCasePass() {
System.out.println("Test Case Passed ");
}

@Test
public void testCaseFail() {
System.out.println("Some Exception occurred!!. Need to fail the test case");
// Assert.fail();
// Assert.fail("Test Case Failed now!!");
Assert.fail("Test Case Failed now!!", new ArithmeticException());
}
}