package Selenium1;

import org.testng.SkipException;
import org.testng.annotations.Test;

public class SampleSkipTestNGTest {

	boolean dataAvailable = false;



	// Test case would be skipped from the execution and won't get shown in the

	// report as well

	@Test(enabled = false)

	public void testCaseDisabling() {

		System.out.println("Please don't execute me. Can't perform anything :) ");

	}



	@Test(enabled = true)

	public void testCaseEnabling() {

		System.out.println("Please execute me. Would perform anything :) ");

	}



	// Test case would be skipped from the execution and but it gets shown in the

	// report as well

	@Test

	public void testCaseSkipException() {

		System.out.println("Im in skip exception");

		throw new SkipException("Skipping this exception");

	}



	// Test case would be skipped from the execution and but it gets shown in the

	// report as well

	@Test

	public void testCaseConditionalSkipException() {

		System.out.println("Im in Conditional Skip");

		if (!dataAvailable)

			throw new SkipException("Skipping this exception");

		System.out.println("Executed Successfully");

	}









  }

