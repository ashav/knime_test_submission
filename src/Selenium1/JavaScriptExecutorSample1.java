package Selenium1;


import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

	

public class JavaScriptExecutorSample1 {
static WebDriver driver;
static JavascriptExecutor js;
static WebElement element;

public static void main(String[] args) throws InterruptedException {
// To launch a Chrome Browser
System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chrome\\chromedriver_win32 (1)\\chromedriver.exe");
// System.setProperty("webdriver.chrome.driver", "C:\\Users\\Pictures\\chromedriver.exe");
driver = new ChromeDriver();

// Creating the JS reference
// TypeCasting done for JavascriptExecutor from driver
js = (JavascriptExecutor) driver;
driver.get("https://en-gb.facebook.com/");
driver.manage().window().maximize();
Thread.sleep(2000);

// Get URL of page
String url = js.executeScript("return document.URL;").toString();
System.out.println("URL : " + url);



// Get innerText of the entire webpage
String innerText = js.executeScript(" return document.documentElement.innerText;").toString();
System.out.println("innerText : " + innerText);

// Refresh the page
js.executeScript("history.go(0)");
Thread.sleep(2000);

// Loading another URL within browser
js.executeScript("window.location = 'https://www.google.com';");
Thread.sleep(2000);

// Close the driver
driver.close();
}
}




