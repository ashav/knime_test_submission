package Selenium1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewTest {
	static WebDriver driver;
	public static final String URL = "https://www.google.com";
	String expectedTitle, actualTitle;

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
	System.out.println("Starting the browser session");

	// To launch a Chrome Browser
	System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chrome\\chromedriver_win32 (1)\\chromedriver.exe");
	driver = new ChromeDriver();

	// Open google.com in Chrome Browser
	driver.get(URL);
	driver.manage().window().maximize();
	Thread.sleep(2000);
	}

@Test
	
	public void firstTest() {
	// Capturing the title and validating if expected is equal to actual
	expectedTitle = "Google";

	// Get the title of the page
	actualTitle = driver.getTitle();

	// Assert whether actualTitle is equal to expectedTitle
	Assert.assertEquals(actualTitle, expectedTitle);
	}

	@AfterMethod
	public void afterTest() {
	System.out.println("Ending the browser session");
	// Closing the browser
	driver.close();
	}

	}

