package Selenium1;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
public class SoftAssertionsExample {
	// SoftAssert softAssert = new SoftAssert();

	// Hard Assertion
	@Test
	public void testCaseOne() {
	System.out.println("*** test case one started ***");
	Assert.assertEquals(5, 5, "First hard assert failed");
	System.out.println("hard assert success....");
	Assert.assertTrue("Hello".equals("hello"), "Second hard assert failed");
	System.out.println("*** test case one executed successfully ***");
	}

	// Soft Assertion
	@Test
	public void testCasetwo() {
	SoftAssert softAssert = new SoftAssert();
	System.out.println("*** test case two started ***");
	// First Soft Assertion
	softAssert.assertEquals("Hello", "Hello", "First soft assert failed");
	System.out.println("hard assert success....");

	// Second Soft Assertion
	softAssert.assertTrue("Hello".equals("hello"), "Second soft assert failed");

	
	// Third Soft Assertion
	softAssert.assertTrue("Welcome".equals("welcomeeee"), "Third soft assert failed");
	System.out.println("*** test case two executed successfully ***");

	// Collating the results of all the Soft Assertions
	softAssert.assertAll();
	}
	}

	//Points to remember : -
	//1. We should instantiate a SoftAssert object within a @Test method. Scope of SoftAssert should only be within the Test method as seen the above example.



