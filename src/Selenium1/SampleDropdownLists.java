package Selenium1;


import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

	public class SampleDropdownLists {
		static WebDriver driver;
		static WebElement element;
		static boolean bool;
		static List<WebElement> list;

		public static void main(String[] args) throws InterruptedException {
		// To launch a Chrome Browser
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chrome\\chromedriver_win32 (1)\\chromedriver.exe");
		driver = new ChromeDriver();

		// Open URL in browser
		driver.get("http://automationpractice.com/index.php?id_category=5&controller=category");
		driver.manage().window().maximize();
		Thread.sleep(2000);

		element = driver.findElement(By.id("selectProductSort"));
		Select select = new Select(element);

		// selectByIndex
		select.selectByIndex(3);
		Thread.sleep(3000);

		// selectByValue
		select.selectByValue("price:asc");
		Thread.sleep(3000);

		
		// selectByVisibleText
		select.selectByVisibleText("Price: Highest first");
		Thread.sleep(3000);

		// GetOptions
		list = select.getOptions();
		System.out.println("All Options : ");
		System.out.println(Arrays.toString(list.toArray()));

		Thread.sleep(3000);
		// getFirstSelectedOption
		select.selectByValue("price:asc");
		System.out.println(" getFirstSelectedOption : " + select.getFirstSelectedOption());

		Thread.sleep(3000);
		// getAllSelectedOptions
		System.out.println(" getAllSelectedOptions ");
		list = select.getAllSelectedOptions();
		System.out.println(Arrays.toString(list.toArray()));

		Thread.sleep(3000);
		// Close the browser
		driver.close();
		}
		}
