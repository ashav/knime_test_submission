package Selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestNGGroupingOfTestCases {
WebDriver driver;
String expectedTitle = "Facebook � log in or sign up";

@Test
public void firstTest() {
System.out.println("This is the starting point of the test");
// Initialize Chrome Driver
System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chrome\\chromedriver_win32 (1)\\chromedriver.exe");
driver = new ChromeDriver();
driver.get("https://en-gb.facebook.com/");
}

@Test(groups = { "sanity" })
public void checkTitle() {
String originalTitle = driver.getTitle();
Assert.assertEquals(originalTitle, expectedTitle);
}

@Test(groups = { "sanity" })
public void click_element() {
driver.findElement(By.xpath("//*[@id='u_0_a_Uk']/div[3]/a")).click();
System.out.println("Home Page heading is displayed");
}
}


