package Selenium1;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class createAndDeleteWorkspace1 { 
	public static WebDriver driver;
	 static WebDriverWait wait;
 public static void main(String[] args) throws InterruptedException {
   String workSpaceName = "Test Workspace";
	System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chrome\\chromedriver_win32 (1)\\chromedriver.exe");
	  driver = new ChromeDriver();
	  wait = new WebDriverWait(driver,30);
   driver.get("https://hub.knime.com/");
   
   driver.manage().window().maximize();

    if(!login()) {
      System.err.println("Login Failed");
      return;
    }

    if(!createWorkspace(workSpaceName)) {
      System.err.println("Couldnt create workspace, aborting.");
      return;
    }

    if(!deleteWorkspace(workSpaceName)) {
      System.err.println("Couldnt delete workspace, aborting.");
    }
    driver.quit();
  }

  static Boolean login() {
    //Accept cookies
    clickAndWait("//button[@class='accept-button button primary']",
        "//div[@class='login']");
    System.out.println("Accepted cookies");

    //User clicks on sign-in
    clickAndWait("//div[@class='login']",
        "//input[@id='edit-name']");
    System.out.println("Clicked on Sign in button");

    //User logs in by giving a valid credentials
    driver.findElement(By.xpath("//input[@id='edit-name']")).sendKeys("NewTestUser");
    System.out.println("Entered username successfully");
    driver.findElement(By.xpath("//input[@id='edit-pass']")).sendKeys("computer88!");
    System.out.println("Entered password successfully");
    System.out.println("clicked on submit button");
    clickAndWait("//button[@id='edit-submit']",
        "//div[@title='newtestuser']");

    //verify if user is successfully logged in
    WebElement userName = driver.findElement(By.xpath("//div[@title='newtestuser']"));
    return userName.isDisplayed();
  }

  static Boolean createWorkspace(String workSpaceName) throws InterruptedException {
    //Steps to access the Spaces
    driver.findElement(By.xpath("//div[@title='newtestuser']")).click();
    clickAndWait("//div[@class='login']//ul/li[2]",
        "//div[@class='create-space-card']//button[2]");
    System.out.println("User is inside workspace location");

    //Steps to create a new public workspace
    clickAndWait("//div[@class='create-space-card']//button[2]",
        "//textarea[@class='text']");
    driver.findElement(By.xpath("//textarea[@class='text']")).sendKeys(workSpaceName);
    driver.findElement(By.xpath("//button[@title='Save']")).click();
    Thread.sleep(5000);// Wait for creation of workspace
    return isWorkspaceAvailable(workSpaceName);
  }
  static boolean isWorkspaceAvailable(String workspaceName) {
    return getWorkspaceId(workspaceName) < 0 ? false:true;
  }
  static Boolean deleteWorkspace(String workSpaceName) throws InterruptedException {
    int workSpaceIndex = getWorkspaceId(workSpaceName);
    if(workSpaceIndex < 0) {
      return false;
    }
    Thread.sleep(5000);
    //Delete the workspace
    clickAndWait("//ul[@class='space-list']//li[" + workSpaceIndex + "]//h3",
        "//button[@class='toggle function-button single']");

    driver.findElement(By.xpath("//button[@class='toggle function-button single']")).click();
    System.out.println("clicked on more options in order to delete the workspace");
    clickAndWait("//div[@class='content']//div[1]//div[1]//button",
        "//form[@id='confirmationForm']//input");

    //Give the workspace name which has to be deleted
    driver.findElement(By.xpath("//form[@id='confirmationForm']//input")).sendKeys(workSpaceName);
    WebElement confirmDeleteButton = driver.findElement(By.xpath("//button[@form='confirmationForm']"));
    Assert.assertTrue(confirmDeleteButton.isEnabled());
    clickAndWait("//button[@form='confirmationForm']",
        "//div[@class='create-space-card']//button[2]");
    return true;
  }

  static int getWorkspaceId(String workSpaceName) {
    //Verify if the workspace is created by going back into the workspace section
    clickAndWait("//nav[@class='breadcrumb']//li[3]",
        "//ul[@class='space-list']");

    WebElement existingWorkSpaces = driver.findElement(By.xpath("//ul[@class='space-list']"));
    List<WebElement> listOfWorkSpaces = existingWorkSpaces.findElements(By.tagName("li"));
    for (int i = 1; i < listOfWorkSpaces.size(); i++) {
      WebElement workspace = driver
          .findElement(By.xpath("//ul[@class='space-list']//li[" + i + "]//h3"));
      if ((workspace.getText()).equals(workSpaceName)) {
        System.out.println("Workspace is successfully created");
        return i;
      }
    }
    return -1;
  }

  static void clickAndWait(String clickPath, String waitPath) {
    driver.findElement(By.xpath(clickPath)).click();
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(waitPath)));
  }
}