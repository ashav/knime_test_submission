package Selenium1;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;



public class TestNGParameterisation1 {
@Parameters("browser")
@Test
public void test1(String browser) {
if (browser.equalsIgnoreCase("FF")) {
System.out.println("The browser value is : " + browser);
} else if (browser.equalsIgnoreCase("Chrome")) {
System.out.println("The browser value is : " + browser);

} else if (browser.equalsIgnoreCase("IE")) {
System.out.println("The browser value is : " + browser);
} else {
System.out.println("Incorrect browser value passed.");
}
}
}
