package Selenium1;



import java.time.Duration;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

public class SampleFluentWaitWithFunction {
static WebDriver driver;
static WebElement element;
static boolean bool;

public static void main(String[] args) throws InterruptedException {
// To launch a Chrome Browser
System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chrome\\chromedriver_win32 (1)\\chromedriver.exe");
driver = new ChromeDriver();

driver.get("https://the-internet.herokuapp.com/tables");
driver.manage().window().maximize();
// Duration duration;

FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
wait.pollingEvery(Duration.ofSeconds(5));
wait.withTimeout(Duration.ofSeconds(20));

// Below exception is ignored even if occurs
wait.ignoring(NoSuchElementException.class);

wait.until(new Function<WebDriver, WebElement>() {
public WebElement apply(WebDriver driver) {
return driver.findElement(By.id("foo"));
}
});


// new FluentWait<WebDriver>(driver).pollingEvery(Duration.ofSeconds(5)).withTimeout(Duration.ofSeconds(20))
// .ignoring(NoSuchElementException.class).until((new Function<WebDriver, WebElement>() {
// public WebElement apply(WebDriver driver) {
// return driver.findElement(By.id("foo"));
// }
// }));

// Close the browser
driver.close();
}
}



