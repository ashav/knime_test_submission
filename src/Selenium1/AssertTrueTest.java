package Selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AssertTrueTest {

	@Test

	public void test1() {

		// To launch a Chrome Browser

		System.setProperty("webdriver.chrome.driver", "/Users/bjahagirdar/Downloads/chromedriver");

		WebDriver driver = new ChromeDriver();

		driver.navigate().to("https://www.spicejet.com/");

		boolean bool = driver.findElement(By.cssSelector("input[id*='SeniorCitizenDiscount']")).isSelected();



		// Expecting the option to be selected by Default .i.e., Value to be 'True'

		Assert.assertTrue(bool);

		System.out.println(driver.findElement(By.cssSelector("input[id*='SeniorCitizenDiscount']")).isSelected());

	}

}







