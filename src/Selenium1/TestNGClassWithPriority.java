package Selenium1;

import org.testng.annotations.Test;

public class TestNGClassWithPriority {



	@Test(priority = 2)

	public void a_method() {

		System.out.println("This is A method");

	}



	@Test(priority = 1)

	public void b_method() {

		System.out.println("This is B method");

	}



	@Test(priority = 4)

	public void c_method() {

		System.out.println("This is C Method");

	}



	@Test(priority = 3)

	public void d_method() {

		System.out.println("This is D Method");

	}

}

