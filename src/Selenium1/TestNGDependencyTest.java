package Selenium1;

import org.testng.annotations.Test;

public class TestNGDependencyTest {

@Test(dependsOnMethods = { "testTwo", "testThree" })
public void testOne() {
System.out.println("Test method one");
}

@Test
public void testTwo() {
System.out.println("Test method two");
// Assert.fail();
}

@Test
public void testThree() {
System.out.println("Test method three");
}
}

//@Test(dependsOnMethods = { "testTwo"})