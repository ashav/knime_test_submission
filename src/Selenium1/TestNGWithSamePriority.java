package Selenium1;

import org.testng.annotations.Test;


public class TestNGWithSamePriority {

	@Test(priority = 1)

	public void a_method() {

		System.out.println("This is A method");

	}



	@Test(priority = 1)

	public void b_method() {

		System.out.println("This is B method");

	}



	// If there is no priority set, it's default value(Priority) is ZERO(0).

	@Test

	public void d_method() {

		System.out.println("This is D Method");

	}



	// If there is no priority set, it's default value(Priority) is ZERO(0).

	@Test

	public void c_method() {

		System.out.println("This is C Method");

	}



	@Test(priority = -1)

	public void e_method() {

		System.out.println("This is E Method");

	}


  }

