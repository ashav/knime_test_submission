package Selenium1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;



public class TestNGParameterisation {
WebDriver driver;
// @Parameters("keyWord")

@Parameters({ "keyWord", "keyWord1" })
@Test
public void Parameterpassing(String keyWord, String keyword1) {
// To launch a Chrome Browser
System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chrome\\chromedriver_win32 (1)\\chromedriver.exe");
driver = new ChromeDriver();
driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);

// Opening Google.com
driver.get("https://google.com");
// Finding search text box
WebElement searchBox = driver.findElement(By.name("q"));
// Sending keyword value
searchBox.sendKeys(keyWord);
System.out.println("The search keyword entered is : " + keyWord);
// Verifying the keyword on UI
Assert.assertTrue(searchBox.getAttribute("value").equalsIgnoreCase(keyWord));
System.out.println("Keywords 1 - Value " + keyword1);
driver.quit();
}
}

