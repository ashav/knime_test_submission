package Selenium1;

import java.awt.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElementSample {
static WebDriver driver;
static WebElement element;
static List listOfElements;

public static void main(String[] args) throws InterruptedException {
// driver.get("http://automationpractice.com/index.php");

// To launch a Chrome Browser
System.setProperty("webdriver.chrome.driver", "/Users/bjahagirdar/Downloads/chromedriver");
driver = new ChromeDriver();

// Open google.com in Chrome Browser
driver.get("https://en-gb.facebook.com/");
driver.manage().window().maximize();
Thread.sleep(2000);

// // NoSuchElementException thrown
// driver.findElement(By.xpath("//input[@type='']"));

// List of Input boxes
// listOfElements = driver.findElements(By.xpath("//input"));
// System.out.println("List of Elements");
// System.out.println(listOfElements);

// List of Empty items
listOfElements = driver.findElements(By.xpath("//input[@type='']"));
System.out.println("empty List : " + listOfElements);

// Closing the browser
driver.close()