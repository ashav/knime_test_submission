package Selenium1;

import java.io.File;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.NoSuchElementException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

	
	public class TakingScreenshotSample {
		static WebDriver driver;
		static JavascriptExecutor js;
		static WebElement element;

		public static void main(String[] args) throws InterruptedException, IOException {
		// To launch a Chrome Browser
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chrome\\chromedriver_win32 (1)\\chromedriver.exe");
		// System.setProperty("webdriver.chrome.driver", "C:\\Users\\Pictures\\chromedriver.exe");
		driver = new ChromeDriver();

		// Creating the JS reference
		js = (JavascriptExecutor) driver;
		driver.get("https://en-gb.facebook.com/");
		driver.manage().window().maximize();
		Thread.sleep(2000);

		// Login flow - Enter the email/password
		js.executeScript("document.getElementById('email').value='Bindacharya';");
		Thread.sleep(2000);

		// Convert web driver reference to TakeScreenshot
		TakesScreenshot screenshot = ((TakesScreenshot) driver);

		// Call 'getScreenshotAs()' method to create an image file
		File SrcFile = screenshot.getScreenshotAs(OutputType.FILE);

		// To append

		
		
		// To append Current timestamp to filename
		String currentTimeStamp = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());

		// Copy file at destination
		FileUtils.copyFile(SrcFile, new File("./ScreenShot_Folder/Test1" + currentTimeStamp + "Login.png"));

		// Close the driver
		driver.close();
		}
		}




