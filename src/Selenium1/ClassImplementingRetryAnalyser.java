package Selenium1;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ClassImplementingRetryAnalyser {
	// @Test
	@Test(retryAnalyzer = RetryAnalyser.class)
	public void intentionallyFailingTest() {
	System.out.println("Executing Test");
	Assert.fail("Failing Test");
	}
	}
