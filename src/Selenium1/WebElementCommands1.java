package Selenium1;

import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class WebElementCommands1 {
		static WebDriver driver;
		static WebElement element;
		static boolean bool;
		static String text;

		public static void main(String[] args) throws InterruptedException {
		// To launch a Chrome Browser
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chrome\\chromedriver_win32 (1)\\chromedriver.exe");
		driver = new ChromeDriver();

		// Open google.com in Chrome Browser
		driver.get("https://en-gb.facebook.com/");
		driver.manage().window().maximize();
		Thread.sleep(2000);

		// // Send keys and Clear Command
		// element = driver.findElement(By.xpath("//input[@id='email']"));
		// element.sendKeys("Typingggg");
		// Thread.sleep(3000);
		// driver.findElement(By.xpath("//input[@id='email']")).clear();
		// Thread.sleep(3000);
		//
		// // Click()/Submit() Command
		// element = driver.findElement(By.id("email"));
		// element.sendKeys("emailID");
		//
		// element = driver.findElement(By.xpath("//input[@type='password']"));
		// element.sendKeys("Password");
		//
		// element = driver.findElement(By

		
		// element = driver.findElement(By.name("login"));
		// element.submit();
		//// element.click();
		//
		// // isDisplayed Command
		// element = driver.findElement(By.id("email"));
		// bool = element.isDisplayed();
		// System.out.println("Is displayed ??" + bool);
		//
		// // isEnabled Command
		// element = driver.findElement(By.id("email"));
		// bool = element.isEnabled();
		// System.out.println("Is Enabled ??" + bool);
		//
		//getText command
		element = driver.findElement(By.name("login"));
		text = element.getText();
		System.out.println("Get Text : " + text);

		//getTagName command
		element = driver.findElement(By.name("login"));
		text = element.getTagName();
		System.out.println("Get Tag : " + text);

		//getAttribute command
		element = driver.findElement(By.name("login"));
		text = element.getAttribute("name");
		System.out.println("Get Attribute : " + text);
		text = element.getAttribute("data-testid");
		System.out.println("Get Attribute : " + text);

		

		// getSize command
		element = driver.findElement(By.name("login"));
		org.openqa.selenium.Dimension dimension = element.getSize();
		System.out.println(dimension.getHeight());
		System.out.println(dimension.getWidth());

		// getLocation command
		element = driver.findElement(By.name("login"));
		Point point = element.getLocation();
		System.out.println(point.getX());
		System.out.println(point.getY());

		// getCssValue
		// CSS property such as background-color, font-family,color, font-weight etc.,
		element = driver.findElement(By.name("login"));
		System.out.println(element.getCssValue("color"));
		System.out.println(element.getCssValue("border-radius"));
		System.out.println(element.getCssValue("font-size"));
		System.out.println(element.getCssValue("font-weight"));

		// Closing the browser
		driver.close();

		}
		}



