package Selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestNGDataProviderTest {

static WebDriver driver;
static WebElement searchBox;

@Test(dataProvider = "searchText")
public void paramByDataProvider(String historicalPlace, String city) throws InterruptedException {
System.setProperty("webdriver.chrome.driver","C:\\Selenium\\chrome\\chromedriver_win32 (1)\\chromedriver.exe");

// Initializing Driver
// To launch a Chrome Browser
driver = new ChromeDriver();

// Opening Google.com in the browser
driver.get("https://google.com");
searchBox = driver.findElement(By.name("q"));
searchBox.sendKeys(historicalPlace + " " + city);
Thread.sleep(2000);
System.out.println("You are trying to search " + historicalPlace + " which is in " + city);
Thread.sleep(2000);

WebElement srchBtn = driver.findElement(By.name("btnK"));
srchBtn.submit();
Thread.sleep(3000);
System.out.println("The page title is: " + driver.getTitle());
driver.quit();
}



// Data Provider which returns Object[][] wherein first column has
// 'historicalPlace' and the seco

@DataProvider(name = "searchText")
public Object[][] getDataProviderData() {
Object[][] searchWords = new Object[3][2];
// Enter data into Object Array
searchWords[0][0] = "India Gate";
searchWords[0][1] = "Delhi";
searchWords[1][0] = "Taj Mahal";
searchWords[1][1] = "Agra";
searchWords[2][0] = "Char Minar";
searchWords[2][1] = "Hyderabad";
return searchWords;

// return new Object[][]{
// { "India Gate", "Delhi" },
// { "Taj Mahal", "Agra" },
// { "Char Minar", "Hyderabad" }
// };
}
}

// 0 1
//0 India Gate Delhi
//1 Taj Mahal Agra
//2 Char Minar Hyderabad
}
