package Selenium1;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SampleTestNGHierarchy {



	@BeforeClass

	public void beforeClass() {

		System.out.println("This will execute before the Class");

	}

	

	@BeforeMethod

	public void beforeMethod() {

		System.out.println();

		System.out.println("This will execute before every Method");

	}



	@Test

	public void testCase1() {

		System.out.println("This is the A Normal Test Case - 1");

	}



	@Test

	public void testCase2() {

		System.out.println("This is the A Normal Test Case - 2");

	}

	

	@Test

	public void testCase10() {

		System.out.println("This is the A Normal Test Case - 10");

	}



	@AfterMethod

	public void afterMethod() {

		System.out.println("This will execute after every Method");

		System.out.println();

	}





	@AfterClass

	public void afterClass() {

		System.out.println("This will execute after the Class");

	}






//It is visible that the @Suite annotation is the very first and the very lastly executed. 

//Then @Test followed by @Class. Now, if you notice, the @Method has run twice. 

//As @Test is a method in the class, hence @Method



//Instantiate any webdriver object - Once - @BeforeClass 



//Opening browser - Multiple times - @BeforeMethod

//Test - one time execution - @Test - 1

//Closing browser - Multiple times - @AfterMethod



//Opening browser - Multiple times - @BeforeMethod

//Test - one time execution - @Test - 2 

//Closing browser - Multiple times - @AfterMethod



//Opening browser - Multiple times - @BeforeMethod

//Test - one time execution - @Test - 3

//Closing browser - Multiple times - @AfterMethod



//Closing the browser completely - Quit - Once - @AfterClass/
  @Test
  public void f() {
  }
}
