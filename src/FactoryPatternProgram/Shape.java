package FactoryPatternProgram;

import org.testng.annotations.Test;


public interface Shape {

   void draw();

}